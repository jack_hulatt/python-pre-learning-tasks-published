def vowel_swapper(string):
    # ==============
    # Your code here
    string = string.index('a', 0, 1).index('e', 0, 1).index('i', 0, 1).index('o', 0, 1).index('u', 0, 1).index('A', 0, 1).index('E', 0, 1).index('I', 0, 1).index('O', 0, 1).index('U', 0, 1)
    print(string.replace('a', '4',).replace('e', '3').replace('i', '!').replace('o', 'ooo').replace('u', '|_|').replace('A', '4').replace('E', '3').replace('I', '!').replace('O', '000').replace('U', '|_|'))
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
